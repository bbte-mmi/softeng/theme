#!/bin/bash

# --------------------------------------------------------
# install theme to local TeXLive distribution
# --------------------------------------------------------

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# find local texmf tree
texmf_home=`kpsewhich -var-value=TEXMFHOME`

# copy everything into local TexMF tree
mkdir -p $texmf_home/tex/latex/ubbsebeamertheme
rm -rf $texmf_home/tex/latex/ubbsebeamertheme
mkdir -p $texmf_home/tex/latex/ubbsebeamertheme
cp -rfv $script_dir/tex/latex/ubbsebeamertheme/* $texmf_home/tex/latex/ubbsebeamertheme

mkdir -p $texmf_home/tex/latex/ubbselab
rm -rf $texmf_home/tex/latex/ubbselab
mkdir -p $texmf_home/tex/latex/ubbselab
cp -rfv $script_dir/tex/latex/ubbselab/* $texmf_home/tex/latex/ubbselab

mkdir -p $texmf_home/fonts
cp -rfv $script_dir/fonts/* $texmf_home/fonts

# rehash so new lib will be found
texhash $texmf_home
mktexmf --all