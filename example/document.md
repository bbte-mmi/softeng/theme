---
theme: ubbse
minted: true

title: Software Engineering
subtitle: Version Control using git
author: Csaba Sulyok
date: Fall 2018
---

# Version control

- common code base for a team
- used to share, review and maintain code
- also useful for backups and automatic builds
- recommended to use the git command line tool **exclusively** while learning *git*
- `this is a bit of code`
